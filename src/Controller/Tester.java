package Controller;

import java.util.ArrayList;
import java.util.Collections;

import Interface.Taxable;
import Model.Company;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.TaxComparator;

public class Tester {
	
	public static void main(String[] args) {
		Tester tester = new Tester();
		tester.testQuestion1a();
		tester.testQuestion1b();
		tester.testQuestion1c();
		tester.testQuestion1d();

	}
	public void testQuestion1a(){
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Rick", 185, 100000));
		persons.add(new Person("Glenn", 180, 500000));
		persons.add(new Person("Sasha", 175, 200000));
		Collections.sort(persons);
		System.out.println("\n---- After sort YearlyIncome");
		for(Person p:persons){
			System.out.println(p);
		}
	}
	
	public void testQuestion1b(){
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("TV", 10000));
		products.add(new Product("Computer", 20000));
		products.add(new Product("Telephone", 5000));
		Collections.sort(products);
		System.out.println("\n---- After sort Price");
		for(Product p:products){
			System.out.println(p);
		}
	}
	
	public void testQuestion1c(){
		ArrayList<Company> companies = new ArrayList<Company>();
		companies.add(new Company("Apple", 1000000, 800000));
		companies.add(new Company("Google", 4000000, 200000));
		companies.add(new Company("Windows", 2000000, 500000));
			
		Collections.sort(companies, new EarningComparator());
		System.out.println("\n---- After sort with Earning");
		for(Company p:companies){
			System.out.println(p);
		}
		
		Collections.sort(companies, new ExpenseComparator());
		System.out.println("\n---- After sort with Expense");
		for(Company p:companies){
			System.out.println(p);
		}
		
		Collections.sort(companies, new ProfitComparator());
		System.out.println("\n---- After sort with Profit");
		for(Company p:companies){
			System.out.println(p);
		}
	}
	
	public void testQuestion1d(){
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		tax.add(new Person("Rick", 185, 100000));
		tax.add(new Person("Glenn", 180, 500000));
		tax.add(new Person("Sasha", 175, 200000));
		tax.add(new Product("TV", 10000));
		tax.add(new Product("Computer", 20000));
		tax.add(new Product("Telephone", 5000));
		tax.add(new Company("Apple", 1000000, 800000));
		tax.add(new Company("Google", 4000000, 200000));
		tax.add(new Company("Windows", 2000000, 500000));
		
		Collections.sort(tax, new TaxComparator());
		System.out.println("\n---- After sort Tax");
		for(Taxable p:tax){
			System.out.println(p);
		}
	}
}
