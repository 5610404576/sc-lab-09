package TreeTraversal;

import java.util.ArrayList;

public interface Traversal {
	public ArrayList<String> traverse(Node node);
}
