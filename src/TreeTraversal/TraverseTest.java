package TreeTraversal;


public class TraverseTest {

	public static void main(String[] args) {
		ReportConsole report = new ReportConsole();
		Node node1 = new Node("A", null, null);
		Node node2 = new Node("C", null, null);
		Node node3 = new Node("E", null, null);
		Node node4 = new Node("D", node2, node3);
		Node node5 = new Node("B", node1, node4);
		Node node6 = new Node("H", null, null);
		Node node7 = new Node("I", node6, null);
		Node node8 = new Node("G", null, node7);
		Node root = new Node("F", node5, node8);

		System.out.print("Traverse with PreOrderTraversal: ");
		report.display(root, new PreOrderTraversal());
		System.out.print("Traverse with InOrderTraversal: ");
		report.display(root, new InOrderTraversal());
		System.out.print("Traverse with PostOrderTraversal: ");
		report.display(root, new PostOrderTraversal());
		
	}

}
