package TreeTraversal;

public class Node{
	public Node left;
	public Node right;
	public String value;
	
	public Node(String value,Node left,Node right){
		this.value = value;
		this.left = left;
		this.right = right;
	}
	public String getValue(){
		return this.value;
	}
	public Node getLeft(){
		return this.left;
	}
	
	public Node getRight(){
		return this.right;
	}

 }