package TreeTraversal;

import java.util.ArrayList;

public class InOrderTraversal implements Traversal {

	@Override
	public ArrayList<String> traverse(Node node) {
		ArrayList<String> inor = new ArrayList<String>();
		if (node == null){
			return inor;
		}

	    inor.addAll(traverse(node.getLeft()));
	    inor.add(node.getValue());
	    inor.addAll(traverse(node.getRight()));

        return inor;
	}

}
