package TreeTraversal;

import java.util.ArrayList;

public class PreOrderTraversal implements Traversal {

	@Override
	public ArrayList<String> traverse(Node node) {
		ArrayList<String> pre = new ArrayList<String>();
		if (node == null){
			return pre;
		}

	    pre.add(node.getValue());
	    pre.addAll(traverse(node.getLeft()));
	    pre.addAll(traverse(node.getRight()));

	    return pre;
		
		
	}



}
