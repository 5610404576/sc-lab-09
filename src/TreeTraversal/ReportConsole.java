package TreeTraversal;

import java.util.ArrayList;


public class ReportConsole {
	
	public void display(Node root,Traversal traversal){
		ArrayList<String> traver = traversal.traverse(root);
		for (String i:traver) {
			System.out.print(i+" ");
		}
		System.out.println();
	}

}
