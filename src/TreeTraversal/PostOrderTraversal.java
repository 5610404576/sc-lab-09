package TreeTraversal;

import java.util.ArrayList;

public class PostOrderTraversal implements Traversal{

	@Override
	public ArrayList<String> traverse(Node node) {
		ArrayList<String> post = new ArrayList<String>();
		if (node == null){
			return post;
		}

	    post.addAll(traverse(node.getLeft()));
	    post.addAll(traverse(node.getRight()));
	    post.add(node.getValue());

        return post;
	}

}
