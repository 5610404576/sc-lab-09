package Model;

import java.util.Comparator;


public class ExpenseComparator implements Comparator<Company>{

	@Override
	public int compare(Company o1, Company o2) {
		double exp1 = o1.getExpenses();
		double exp2 = o2.getExpenses();
		if (exp1 > exp2) {
			return 1;
		}
		if (exp1 < exp2){
			return -1;
		}
		return 0;
	}

}
