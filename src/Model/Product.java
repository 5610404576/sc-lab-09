package Model;


import Interface.Taxable;

public class Product implements Taxable, Comparable<Product> {
	private String name;
	private double price;

	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return this.name;
	}

	public double getPrice() {
		return this.price;
	}

	@Override
	public double getTax() {
		return 0.07 * this.getPrice();
	}

	@Override
	public int compareTo(Product other) {
		if (this.getPrice() < other.getPrice() ) { 
			return -1; 
		}
		if (this.getPrice() > other.getPrice() ) {
			return 1;  
		}
		return 0;
	}
	
	public String toString(){
		return "Product["+getName()+" Price: "+getPrice()+" Tax: "+getTax()+"]";
	}
}
