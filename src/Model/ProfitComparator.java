package Model;

import java.util.Comparator;


public class ProfitComparator implements Comparator<Company>{

	@Override
	public int compare(Company o1, Company o2) {
		double profit1 = o1.getProfit();
		double profit2 = o2.getProfit();
		if (profit1 > profit2) {
			return 1;
		}
		if (profit1 < profit2){
			return -1;
		}
		return 0;
	}

}
