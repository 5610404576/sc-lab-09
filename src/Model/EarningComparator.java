package Model;

import java.util.Comparator;



public class EarningComparator implements Comparator<Company>{

	@Override
	public int compare(Company o1, Company o2) {
		double income1 = o1.getIncome();
		double income2 = o2.getIncome();
		if (income1 > income2) {
			return 1;
		}
		if (income1 < income2){
			return -1;
		}
		return 0;
	}

}

